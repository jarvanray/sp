package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jarvan
 * 2020/8/31 10:54
 */
@Component
public class UserFeignClientFB implements UserFeignClient{


    @Override
    public JsonResult<User> getUser(Integer userId) {
        if (Math.random()<0.5){
            User user = new User(userId,"用户名"+userId,"密码"+userId);
            return JsonResult.ok().data(user);
        }
        return JsonResult.err().msg("获取用户失败");
    }

    @Override
    public JsonResult addSorce(Integer userId, Integer score) {
        return JsonResult.err().msg("增加用户积分失败");
    }
}
