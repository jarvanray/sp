package cn.tedu.sp09.feign;

import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.web.util.JsonResult;
import org.springframework.stereotype.Component;

/**
 * @author Jarvan
 * 2020/8/29 14:10
 */
@Component
public class OrderFeignClientFB implements OrderFeignClient{
    @Override
    public JsonResult<Order> getOrder(String orderId) {
        return JsonResult.err().msg("获取订单列表失败");
    }

    @Override
    public JsonResult addOrder() {
        return JsonResult.err().msg("新增订单失败");
    }
}
