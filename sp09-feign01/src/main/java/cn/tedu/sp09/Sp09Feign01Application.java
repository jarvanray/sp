package cn.tedu.sp09;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sp09Feign01Application {

    public static void main(String[] args) {
        SpringApplication.run(Sp09Feign01Application.class, args);
    }

}
