package cn.tedu.sp01.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;

import java.util.List;

/**
 * @author Jarvan
 * 2020/8/25 10:25
 */
public interface OrderService {
    Order getOrder(String orderId);
    void addOrder(Order order);
}
