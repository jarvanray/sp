package cn.tedu.sp01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author Jarvan
 * 2020/8/25 10:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String id;
    private User user;
    private List<Item> items;
}
